#pragma once

#define _CRTDBG_MAP_ALLOC
#include <vector>
#include <random>

struct State {
	// Parameters
	std::vector<bool> configuration_m;
	int price_m = 0;
	int weight_m = 0;

	// Constructor
	State() {
		configuration_m = {};
		price_m = 0;
		weight_m = 0;
	}

	// Methods 
	void printState() {
		for (int i = 0; i < configuration_m.size(); ++i) {
			std::cout << configuration_m[i] << " ";
		}
		std::cout << std::endl;
	}

	void initializeStateWithRandomValues(int size) {
		std::random_device dev;
		std::mt19937 rng(dev());

		for (int i = 0; i < size; ++i) {
			std::uniform_real_distribution<double> dist(0, 1);
			if (dist(rng) < 0.5f) {
				this->configuration_m.push_back(false);
			} else {
				this->configuration_m.push_back(true);
			}
		}
	}

	void generateRandomNeighbour(int size) {
		std::random_device dev;
		std::mt19937 rng(dev());
		std::uniform_int_distribution<int> dist(0, size - 1);

		int position = dist(rng);
		if (configuration_m[position]) {
			configuration_m[position] = false;
		} else {
			configuration_m[position] = true;
		}
	}

	int getCost(int numberOfItems, int bagCapacity) {
		if (weight_m > bagCapacity) {
			return (bagCapacity - weight_m);
		} else {
			return price_m/10;
		}
	}
};
