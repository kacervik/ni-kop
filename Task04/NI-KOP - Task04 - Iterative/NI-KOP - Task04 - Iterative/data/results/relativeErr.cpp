#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>

int main(int argc, char** argv) {
	std::string outputFileName = "relErrors37.txt";
	std::ofstream outputFile(outputFileName);

	std::string inputOGName = "NK37_sol.dat";
	std::string inputResName = "SA-37-default.txt";

	std::ifstream inputFileOG;
	std::ifstream inputFileRes;

	inputFileOG.open(inputOGName, std::ios::in);
	inputFileRes.open(inputResName, std::ios::in);

	int xOG = 0, xRES = 0;
	std::string y;
	std::vector<double> errors;
	int lastID = -1;

	for (int i = 0; i < 500; ++i) {
		inputFileOG >> xOG;
		if (xOG == lastID) {
			while (xOG == lastID) {
				std::getline(inputFileOG, y);
				inputFileOG >> xOG;
			}
			lastID = xOG;
			inputFileOG >> xOG >> xOG;
			std::getline(inputFileOG, y);
		} else {
			lastID = xOG;
			inputFileOG >> xOG >> xOG;
			std::getline(inputFileOG, y);
		}

		inputFileRes >> xRES >> xRES >> xRES;
		std::getline(inputFileRes, y);

		if (xOG == 0 || xRES == 0) {
			std::cout << i << std::endl;
			errors.push_back(0);
		} else if (xOG > xRES) {
			std::cout << i << " " << (abs(xOG - xRES)) << " " << (double)xOG << std::endl;
			errors.push_back((abs(xOG - xRES)) / (double)xOG);
		} else {
			std::cout << i << " " << (abs(xOG - xRES)) << " " << (double)xRES << std::endl;
			errors.push_back((abs(xOG - xRES)) / (double)xRES);
		}
	}

	double max = 0, min = 1;
	for (double error : errors) {
		if (error > max) {
			max = error;
		}

		if (error < min) {
			min = error;
		}
	}

	double avg = 1.0 * std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size();
	outputFile << min << std::endl;
	outputFile << avg << std::endl;
	outputFile << max << std::endl;
	errors.clear();
		
	outputFile.close();

	return 0;
}