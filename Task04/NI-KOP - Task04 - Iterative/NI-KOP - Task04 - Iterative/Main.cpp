// NI-KOP - Task2 - Problem Batohu.cpp : Defines the entry point for the application.
//

// #include <crtdbg.h>
#include "Bag.h"
#include "Timer.h"

/*
	Read the file and process it in brute force calculations.
*/
bool processFile(std::ifstream& inputFile, const std::string& inputFileName) {
	inputFile.open(inputFileName, std::ios::in);
	if (!inputFile.is_open() || !inputFile) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	// TODO change method name
	double eps = 0.50;

	// std::string outputFileNameResults = inputFileName.substr(0, inputFileName.length() - 4) + "_res.txt";
	// std::string outputFileNameResults= inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_res.txt";
	// std::ofstream outputFileResults(outputFileNameResults);

	// std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 4) + "_time.txt";
	// std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_time.txt";
	// std::ofstream outputFileTime(outputFileNameTime);

	std::string outputFileNameResults = "data/results/SA-32-default.txt";
	std::ofstream outputFile(outputFileNameResults);

	while (inputFile.good()) {
		Bag bag;
		Item tmpItem;

		inputFile >> bag.ID >> bag.numberOfItems >> bag.bagCapacity;
		if (bag.ID < 0) {
			inputFile >> bag.minimalPriceOfItems;
		}

		for (int i = 0; i < bag.numberOfItems; ++i) {
			inputFile >> tmpItem.weight >> tmpItem.price;
			bag.items.push_back(tmpItem);
		}

		if (!inputFile.eof()) {
			Timer stopwatch;
			State bestState, state;
			for (int i = 0; i < 1; ++i) {
				state = bag.simulatedAnealing(outputFile);
				if (state.getCost(bag.numberOfItems, bag.bagCapacity) > bestState.getCost(bag.numberOfItems, bag.bagCapacity)) {
					bestState = state;
				}
			}

			// std::cout <<  bag.ID << " " << bag.numberOfItems << " " << bestState.price_m << " ";
			// state.printState();

			// outputFileTime << (long long)(stopwatch.getElapsedTime()/3.0f) << std::endl;
		}
	}

	// outputFileResults.close();
	outputFile.close();
	return true;
}


int main(int argc, char** argv) {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	std::string inputFileName;
	inputFileName = "data\\NK32_inst.dat";

	std::ifstream inputFile;
	if (!processFile(inputFile, inputFileName)) {
		std::cout << "There was a problem processing your file." << std::endl;
		inputFile.close();
		return 1;
	}
	inputFile.close();


	return 0;
}
