
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>

int main(int argc, char** argv) {
	std::vector<std::string> instances = {"04", "10", "15", "20", "22", "25", "27", "30", "32", "35", "37", "40"};
	std::string inputOGName;
	std::string inputResName;

	std::string outputFileMaxName = "ZKC\\ZKC_Greedy_error_max.txt";
	std::ofstream outputFileMax(outputFileMaxName);

	std::string outputFileAvgName = "ZKC\\ZKC_Greedy_error_avg.txt";
	std::ofstream outputFileAvg(outputFileAvgName);

	for (std::string number : instances) {
		inputOGName = "ZKC\\ZKC" + number + "_sol.dat";
		inputResName = "ZKC\\ZKC" + number + "_Greedy_res.txt";

		std::ifstream inputFileOG;
		std::ifstream inputFileRes;

		inputFileOG.open(inputOGName, std::ios::in);
		inputFileRes.open(inputResName, std::ios::in);

		int xOG = 0, xRES = 0;
		double x;
		std::string y;
		std::vector<double> errors;
		int lastID = -1;

		for (int i = 0; i < 500; ++i) {
			// inputFileOG >> xOG >> xOG >> xOG;
			inputFileOG >> xOG;
			if (xOG == lastID) {
				while (xOG == lastID) {
					std::getline(inputFileOG, y);
					inputFileOG >> xOG;
				}
				lastID = xOG;
				inputFileOG >> xOG >> xOG;
				std::getline(inputFileOG, y);
			} else {
				lastID = xOG;
				inputFileOG >> xOG >> xOG;
				std::getline(inputFileOG, y);
			}

			inputFileRes >> xRES >> xRES >> xRES;
			std::getline(inputFileRes, y);

			if (xOG == 0 || xRES == 0) {
				errors.push_back(0);
			} else if (xOG > xRES) {
				errors.push_back((abs(xOG - xRES)) / (double)xOG);
			} else {
				errors.push_back((abs(xOG - xRES)) / (double)xRES);
			}
		}

		double max = 0;
		for (double error : errors) {
			if (error > max) {
				max = error;
			}
		}

		double avg = 1.0 * std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size();
		outputFileAvg << avg << std::endl;
		outputFileMax << max << std::endl;
		errors.clear();
	}

	return 0;
}