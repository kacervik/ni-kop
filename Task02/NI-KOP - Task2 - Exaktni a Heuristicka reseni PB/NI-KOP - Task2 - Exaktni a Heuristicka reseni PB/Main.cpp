// NI-KOP - Task2 - Problem Batohu.cpp : Defines the entry point for the application.
//

// #include <crtdbg.h>
#include "Bag.h"
#include "Timer.h"

/*
	Read the file and process it in brute force calculations.
*/
bool processFile(std::ifstream& inputFile, const std::string& inputFileName) {
	inputFile.open(inputFileName, std::ios::in);
	if (!inputFile.is_open() || !inputFile) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	// TODO change method name
	double eps = 0.05;

	std::string outputFileNameResults = inputFileName.substr(0, inputFileName.length() - 8) + "Redux_res.txt";
	// std::string outputFileNameResults= inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_res.txt";
	std::ofstream outputFileResults(outputFileNameResults);

	std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 8) + "Redux_time.txt";
	// std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_time.txt";
	std::ofstream outputFileTime(outputFileNameTime);
	while (inputFile.good()) {
		Bag bag;
		Item tmpItem;

		inputFile >> bag.ID >> bag.numberOfItems >> bag.bagCapacity;
		if (bag.ID < 0) {
			inputFile >> bag.minimalPriceOfItems;
		}

		for (int i = 0; i < bag.numberOfItems; ++i) {
			inputFile >> tmpItem.weight >> tmpItem.price;
			bag.items.push_back(tmpItem);
		}

		if (!inputFile.eof()) {
			Timer stopwatch;
			for (int i = 0; i < 3; ++i) {
				// bag.bruteForceMethod_KnapsackProblem();

				// bag.branchAndBoundsMethod_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveBnBToFile(outputFileResults);
				// }
				// bag.printResult_ConstructiveAlgorithm();

				// bag.greedyMethod_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveResult_GreedyAlgorithm(outputFileResults);
				// }
				// bag.printResult_GreedyAlgorithm();

				int reduxResult = bag.greedyReduxMethod_KnapsackProblem();
				if (i == 0) {
					bag.saveResult_ReduxAlgorithm(reduxResult, outputFileResults);
				}

				// bag.dynamicProgramming_KnapsackProblem(bag.items);
				// bag.FPTAS_KnapsackProblem(eps);
				// if (i == 0) {
				// 	bag.saveResult_DynamicProgramming(outputFileResults);
				// }
				// bag.printResult_DynamicProgramming();
			}
			outputFileTime << (long long)(stopwatch.getElapsedTime()/3.0f) << std::endl;
		}
	}

	outputFileResults.close();
	return true;
}


int main(int argc, char** argv) {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	std::vector<std::string> instances = {"04", "10", "15", "20", "22", "25", "27", "30", "32", "35", "37", "40"};
	std::string inputFileName;

	for (std::string number : instances) {
		if (argv[1]) {
			inputFileName = argv[1];
			inputFileName = inputFileName.substr(0, inputFileName.length() - 7) + number + "_inst.dat";
			// inputFileName = inputFileName + ".dat";
		} else {
			std::cout << "File name was not specified." << std::endl;
			return 1;
		}

		std::ifstream inputFile;
		if (!processFile(inputFile, inputFileName)) {
			std::cout << "There was a problem processing your file." << std::endl;
			inputFile.close();
			return 1;
		}
		inputFile.close();
	}

	return 0;
}
