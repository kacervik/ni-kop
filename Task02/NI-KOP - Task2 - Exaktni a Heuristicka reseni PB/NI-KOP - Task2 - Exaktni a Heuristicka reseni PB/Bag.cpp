#include "Bag.h"

// Compares ratios.
bool cmpRatio(GreedyItem& a, GreedyItem& b) {
	return (a.ratio > b.ratio);
}

// Compares order.
bool cmpOrder(GreedyItem& a, GreedyItem& b) {
	return (a.originalPosition < b.originalPosition);
}

// Returns linear position from 2D coordinates.
long int Bag::pos(int a, int b) {
	return a * sumOfItems + b;
}

/*
	Print what is stored in the bag.
	ID, number of items, capacity of the bag and the minimal price for items.
*/
void Bag::printBagContents() {
	std::cout << Bag::ID << " " << Bag::numberOfItems << " " << Bag::bagCapacity << " " << Bag::minimalPriceOfItems << " - ";
	for (Item item : Bag::items) {
		std::cout << item.weight << " " << item.price << " ";
	}
	std::cout << std::endl;
}


/*
	Print the results of constructive algorithm in form
	ID, number of items, maximal Reached Price (with the lowest weight), solution (configuration)
*/
void Bag::printResult_ConstructiveAlgorithm() {
	std::cout << ID << " " << numberOfItems << " " << maximalReachedPrice << " ";
	for (int cnfg : configurationBnB) {
		std::cout << cnfg << " ";
	}
	std::cout << std::endl;
}


/*
	Save the result of BnB calculations to file in input parameter in format of
	ID, number of items, maximal Reached Price (with the lowest weight), solution (configuration)
*/
void Bag::saveBnBToFile(std::ofstream& outputFile) {
	outputFile << ID << " " << numberOfItems << " " << maximalReachedPrice << " ";;
	for (int cnfg : configurationBnB) {
		outputFile << cnfg << " ";
	}
	outputFile << std::endl;
}


/*
	Create binary tree by brute force to find the ideal configuration.
*/
void Bag::bruteForceMethod_KnapsackProblem() {
	Item item;
	std::vector<bool> config;

	buildTreeBruteForce(item, 0, config);
	printResult_ConstructiveAlgorithm();
}


/*
	Recursively build tree with brute force method.
	Each left branch does not add new item.
	Each right branch does add new item dependant on the depth of the tree.
*/
void Bag::buildTreeBruteForce(Item item, int depth, std::vector<bool> config) {
	// If item is already over limit, dont continue.
	if (item.weight > bagCapacity) {
		return;
	}

	// Check if we are adding the last item.
	if (depth == numberOfItems) {
		if (item.price >= maximalReachedPrice) {
			if (item.price == maximalReachedPrice) {
				if (item.weight < currentMinWeightForMaxPrice) {
					maximalReachedPrice = item.price;
					currentMinWeightForMaxPrice = item.weight;
					configurationBnB = config;
				}
			} else {
				maximalReachedPrice = item.price;
				currentMinWeightForMaxPrice = item.weight;
				configurationBnB = config;
			}
		}

		return;
	}

	// Left branch
	config.push_back(false);
	buildTreeBruteForce(item, depth + 1, config);

	// Right branch
	item.price += items[depth].price;
	item.weight += items[depth].weight;
	config.pop_back();
	config.push_back(true);
	buildTreeBruteForce(item, depth + 1, config);

	return;
}


/*
	Create binary tree for BnB to find ideal configuration
*/
void Bag::branchAndBoundsMethod_KnapsackProblem() {
	Item item;
	std::vector<bool> config;

	buildTreeBnB(item, 0, config);
}


/*
	Recursively build tree with BnB method.
	Each left branch does not add new item.
	Each right branch does add new item dependant on the depth of the tree.
*/
void Bag::buildTreeBnB(Item item, int depth, std::vector<bool> config) {
	// If item is already over limit, dont continue.
	if (item.weight > bagCapacity) {
		return;
	}

	// Check if we are adding the last item.
	if (depth == numberOfItems) {
		if (item.price >= maximalReachedPrice) {
			if (item.price == maximalReachedPrice) {
				if (item.weight < currentMinWeightForMaxPrice) {
					maximalReachedPrice = item.price;
					currentMinWeightForMaxPrice = item.weight;
					configurationBnB = config;
				}
			} else {
				maximalReachedPrice = item.price;
				currentMinWeightForMaxPrice = item.weight;
				configurationBnB = config;
			}
		}
		return;
	}

	/*
		Branchand Bounds
		Chceck if adding aditional items will increase the price
		if the price will be same, check also weight
	*/
	int potentialPrice = 0, potentialWeight = 0;
	for (int i = depth; i < numberOfItems; ++i) {
		potentialPrice += items[i].price;
		potentialWeight += items[i].weight;
	}

	if (item.price + potentialPrice < maximalReachedPrice) {
		return;
	}

	if (item.price + potentialPrice == maximalReachedPrice && item.weight + potentialWeight > currentMinWeightForMaxPrice) {
		return;
	}

	// Left branch
	config.push_back(false);
	buildTreeBnB(item, depth + 1, config);

	// Right branch
	item.price += items[depth].price;
	item.weight += items[depth].weight;
	config.pop_back();
	config.push_back(true);
	buildTreeBnB(item, depth + 1, config);

	return;
}


/*
	Constructs greedy order based on ratios and determines which items are added later.
*/
void Bag::greedyMethod_KnapsackProblem(bool fromRedux) {
	constructGreedyOrder(fromRedux);
	determineSuitableItemsInGreedyOrder();
}


/*
	Algorithm for constructing vector of ordered greedy items.
	Input bool fromRedux - 
		if this algorithm is called from redux method, we need to also find the item with highest price. (and lowest weight)
		else we just need to construct greedy order.
*/
void Bag::constructGreedyOrder(bool fromRedux) {
	greedyItems.clear();

	if (fromRedux) {
		for (int i = 0; i < numberOfItems; ++i) {
			GreedyItem tmpGI;

			tmpGI.originalPosition = i;
			tmpGI.ratio = (double)items[i].price / (double)items[i].weight;
			greedyItems.push_back(tmpGI);

			// Saving item with highest price, if the prices are same, save the one with lover weight.
			if (items[i].price >= maximalReachedPrice && items[i].weight <= bagCapacity) {
				if (items[i].price == maximalReachedPrice) {
					if (items[i].weight < currentMinWeightForMaxPrice) {
						maximalReachedPrice = items[i].price;
						currentMinWeightForMaxPrice = items[i].weight;
						indexOfItemWithMaxPriceAndMinWeight = i;
					}
				} else {
					maximalReachedPrice = items[i].price;
					currentMinWeightForMaxPrice = items[i].weight;
					indexOfItemWithMaxPriceAndMinWeight = i;
				}
			}
		}
	} else {
		for (int i = 0; i < numberOfItems; ++i) {
			GreedyItem tmpGI;

			tmpGI.originalPosition = i;
			tmpGI.ratio = (double)items[i].price / (double)items[i].weight;
			greedyItems.push_back(tmpGI);
		}
	}
	

	std::sort(greedyItems.begin(), greedyItems.end(), cmpRatio);
}


/*
	Iteratively go through the vector and mark greedy items as added
	if they still fit in the bag capacity.
	Goes from highest value ratio to the lowest.
*/
void Bag::determineSuitableItemsInGreedyOrder() {
	int remainingCapacity = bagCapacity;

	for (int i = 0; i < numberOfItems; ++i) {
		if (items[greedyItems[i].originalPosition].weight <= remainingCapacity) {
			greedyItems[i].isAdded = true;
			remainingCapacity -= items[greedyItems[i].originalPosition].weight;
		}
	}
}


/*
	Prints results of a greedy algorithm.
*/
void Bag::printResult_GreedyAlgorithm() {
	int price = 0;
	for (const GreedyItem& x : greedyItems) {
		if (x.isAdded)
			price += items[x.originalPosition].price;
	}
	std::cout << ID << " " << numberOfItems << " " << price << " ";

	std::sort(greedyItems.begin(), greedyItems.end(), cmpOrder);
	for (const GreedyItem& x : greedyItems) {
		std::cout << x.isAdded << " ";
	}

	std::cout << std::endl;
}

/*
	Save results of greedy algorithm into a file.
*/
void Bag::saveResult_GreedyAlgorithm(std::ofstream& outputFile) {
	int price = 0;
	for (const GreedyItem& x : greedyItems) {
		if (x.isAdded)
			price += items[x.originalPosition].price;
	}
	outputFile << ID << " " << numberOfItems << " " << price << " ";

	std::sort(greedyItems.begin(), greedyItems.end(), cmpOrder);
	for (const GreedyItem& x : greedyItems) {
		outputFile << x.isAdded << " ";
	}

	outputFile << std::endl;
}

/*
	Constructs greedy method as well as find the item with highest price/lowest weight and compares two methods.
*/
int Bag::greedyReduxMethod_KnapsackProblem() {
	greedyMethod_KnapsackProblem(true);
	return compareGreedyAndReduxMethods();
}


/*
	Goes through greedy items and sums up their price and weight
	then it is compared with single item from redux method and result is returned.
	return
		0 - greedy gives better result
		1 - redux gives the same result
		2 - no item is suitable in config, non of them can be added
*/
int Bag::compareGreedyAndReduxMethods() {
	if (indexOfItemWithMaxPriceAndMinWeight == -1) {
		return 2;
	}

	int greedyPrice = 0, 
		greedyWeight = 0;
	int reduxPrice = items[indexOfItemWithMaxPriceAndMinWeight].price, 
		reduxWeight = items[indexOfItemWithMaxPriceAndMinWeight].weight;

	for (int i = 0; i < numberOfItems; ++i) {
		if (greedyItems[i].isAdded) {
			greedyPrice += items[greedyItems[i].originalPosition].price;
			greedyWeight += items[greedyItems[i].originalPosition].weight;
		}
	}

	if (greedyPrice < reduxPrice) {
		return 1;
	} else if (greedyPrice > reduxPrice) {
		return 0;
	} else {
		if (reduxWeight <= greedyWeight) {
			return 1;
		} else {
			return 0;
		}
	}
}


/*
	Prints result of greedy algorithm and stores them in the outputFile
*/
void Bag::saveResult_ReduxAlgorithm(int result, std::ofstream& outputFile) {
	if (result == 1) {
		outputFile << ID << " " << numberOfItems << " " << items[indexOfItemWithMaxPriceAndMinWeight].price << " ";
		for (int i = 0; i < numberOfItems; ++i) {
			if (i == indexOfItemWithMaxPriceAndMinWeight) {
				outputFile << 1 << " ";
			} else {
				outputFile << 0 << " ";
			}
		} outputFile << std::endl;

	} else if (result == 0) {
		int price = 0;
		for (const GreedyItem& x : greedyItems) {
			if (x.isAdded)
				price += items[x.originalPosition].price;
		}
		outputFile << ID << " " << numberOfItems << " " << price << " ";

		std::sort(greedyItems.begin(), greedyItems.end(), cmpOrder);
		for (const GreedyItem& x : greedyItems) {
			outputFile << x.isAdded << " ";
		} outputFile << std::endl;

	} else {
		outputFile << ID << " " << numberOfItems << " " << 0 << " ";
		for (int i = 0; i < numberOfItems; ++i) {
			outputFile << 0 << " ";
		} outputFile << std::endl;
	}
}


/*
	Creates the table and fills it in with correct values.
	Finds the best cell with weight/price from the last column.
	And creates configuration, that got us to that cell.
	Configuration is stored in Bag::configurationDynamic.
	Input is vector of items, to acomodate for normal dynamic programming method and FPTAS.
*/
void Bag::dynamicProgramming_KnapsackProblem(std::vector<Item> items) {
	for (Item item : items) {
		sumOfItems += item.price;
	} sumOfItems += 1; // we need space for 0 in bottom row.

	// Create full table with no space saving features.
	int* table = new int[(itemsFTPAS.size() + 1) * sumOfItems]();

	// Fill first column with infinities, but [0, 0] = 0
	for (int i = 1; i < sumOfItems; ++i) {
		table[pos(0, i)] = INT_MAX;
	}
	
	// Construct the table with weights.
	// For each column, go through the all cells. (bottom/up)
	for (int i = 1; i < (itemsFTPAS.size() + 1); ++i) {
		for (int p = 1; p < sumOfItems; ++p) {

			// Check if the price is lesser than the indexed price
			// if it is, calculate the cell weight with dynamic programming.
			// else put weight of left cell to current one.
			if (items[i - 1].price <= p) {
				int x = table[pos(i - 1, p)];  // weight of the left cell
				int y = items[i - 1].weight + table[pos(i - 1, p - items[i - 1].price)]; // Weight of current item plus weights of the cells bellow (bottom/up)

				if (y < 0) { // Replace the current cell with the lesser weight. (If one cell overflows, )
					table[pos(i, p)] = table[pos(i - 1, p)];
				} else {
					table[pos(i, p)] = std::min(x, y);
				}
			} else {
				table[pos(i, p)] = table[pos(i - 1, p)];
			}
		}
	}

	// Find the cell with highest index and weight <= bag capacity
	int cell = -1;
	configurationDynamic.clear();
	for (int i = ((itemsFTPAS.size() + 1) * sumOfItems) - 1; i > ((itemsFTPAS.size() + 1) * sumOfItems) - sumOfItems; --i) {
		if (table[i] <= bagCapacity) {
			cell = i;
			break;
		}
	}

	// If cell is set do default value, that means there is no good configuration
	if (cell == -1) {
		for (int i = 0; i < numberOfItems; ++i) {
			configurationDynamic.push_back(false);
		}
		delete[] table;
		return;
	}

	int i = itemsFTPAS.size() - 1;
	for (; i >= 0; --i) {
		// If left cell is same as current, add 0 to config and move to left cell
		// else move to the cell that got us to current cell
		if (table[cell] == table[cell - sumOfItems]) {
			configurationDynamic.push_back(false);
			cell -= sumOfItems;
		} else {
			configurationDynamic.push_back(true);
			cell = cell - items[i].price - sumOfItems;
		}
	}

	// Reverse the configuration, because we were making it from the back to the front.
	std::reverse(configurationDynamic.begin(), configurationDynamic.end());
	delete[] table;
}


/*
	Prints results of dynamic programming from vector configurationDynamic.
	Format: ID, number of items, sum of items, configuration
*/
void Bag::printResult_DynamicProgramming() {
	int sum = 0;
	for (int i = 0; i < numberOfItems; ++i) {
		if (configurationDynamic[i]) {
			sum += items[i].price;
		}
	}

	std::cout << ID << " " << numberOfItems << " " << sum << " ";
	for (int cnfg : configurationDynamic) {
		std::cout << cnfg << " ";
	}
	std::cout << std::endl;
}


/*
	Saves results of dynamic programming from vector configurationDynamic to output file.
	Format: ID, number of items, sum of items, 
*/
void Bag::saveResult_DynamicProgramming(std::ofstream& outputFile) {
	int sum = 0;
	for (int i = 0; i < itemsFTPAS.size(); ++i) {
		if (configurationDynamic[i]) {
			sum += items[i].price;
		}
	}

	outputFile << ID << " " << numberOfItems << " " << sum << " ";
	// for (int cnfg : configurationDynamic) {
	// 	outputFile << cnfg << " ";
	// }

	for (int i = 0; i < numberOfItems; ++i) {
		if (i < configurationDynamic.size()) {
			outputFile << configurationDynamic[i] << " ";
		} else {
			outputFile << 0 << " ";
		}

	}
	outputFile << std::endl;
}


/*
	Creates a Item vector with cut down prices and sends it to the dynamic programming algorithm.
	Parameter eps dictates the error/bit cutting.
*/
void Bag::FPTAS_KnapsackProblem(double eps) {
	int maxPrice = 0;
	int n = 0;
	for (int i = 0; i < numberOfItems; ++i) {
		if (items[i].weight <= bagCapacity) {
			if (items[i].price > maxPrice) {
				maxPrice = items[i].price;
			}
			++n;
		}
	}

	if (n != 0) {
		double K = (eps * maxPrice) / (n);

		itemsFTPAS.clear();
		for (int i = 0; i < numberOfItems; ++i) {
				Item tmpItem;
				tmpItem.price = items[i].price / K;
				tmpItem.weight = items[i].weight;
				itemsFTPAS.push_back(tmpItem);
		}

		dynamicProgramming_KnapsackProblem(itemsFTPAS);
	} else {
		for (int i = 0; i < numberOfItems; ++i) {
			configurationDynamic.push_back(false);
		}
	}
}
