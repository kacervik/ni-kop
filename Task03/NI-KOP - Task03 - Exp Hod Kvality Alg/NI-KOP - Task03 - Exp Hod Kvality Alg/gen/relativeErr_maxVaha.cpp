#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>

int main(int argc, char** argv) {
	// std::vector<std::string> instances = {"4", "8", "12", "16", "20", "24", "28"};
	// std::vector<std::string> instances = {"0.15", "0.30", "0.45", "0.60", "0.75", "0.90"};
	// std::vector<std::string> instances = {"100", "250", "400", "550", "700", "850", "1000"};
	std::vector<std::string> instances = {"10", "25", "100", "150", "200", "350", "500", "750", "1000"};
	// std::vector<std::string> instances = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
	// std::vector<std::string> instances = {"0.25", "0.5", "0.75", "1", "1.25", "1.5", "1.75", "2", "2.5", "3", "3.5", "4", "5", "6", "7"};

	// std::vector<std::string> types = {"bal", "light", "heavy"};
	// std::vector<std::string> types = {"corr", "strong", "uni"};
	std::vector<std::string> types = {""};

	std::string inputOGName;
	std::string inputResName;

	for (std::string type : types) {
		std::string outputFileMaxName = "maxVaha//Greedy_" + type + "_error_max.txt";
		std::ofstream outputFileMax(outputFileMaxName);

		std::string outputFileAvgName = "maxVaha//Greedy_" + type + "_error_avg.txt";
		std::ofstream outputFileAvg(outputFileAvgName);

		for (std::string number : instances) {
			inputOGName = "maxVaha//" + type + number + "_DP_res.txt";
			inputResName = "maxVaha//" + type + number + "_GR_res.txt";

			std::ifstream inputFileOG;
			std::ifstream inputFileRes;

			inputFileOG.open(inputOGName, std::ios::in);
			inputFileRes.open(inputResName, std::ios::in);

			int xOG = 0, xRES = 0;
			double x;
			std::string y;
			std::vector<double> errors;
			int lastID = -1;

			for (int i = 0; i < 500; ++i) {
				// inputFileOG >> xOG >> xOG >> xOG;
				inputFileOG >> xOG;
				if (xOG == lastID) {
					while (xOG == lastID) {
						std::getline(inputFileOG, y);
						inputFileOG >> xOG;
					}
					lastID = xOG;
					inputFileOG >> xOG >> xOG;
					std::getline(inputFileOG, y);
				} else {
					lastID = xOG;
					inputFileOG >> xOG >> xOG;
					std::getline(inputFileOG, y);
				}

				inputFileRes >> xRES >> xRES >> xRES;
				std::getline(inputFileRes, y);

				if (xOG == 0 || xRES == 0) {
					errors.push_back(0);
				} else if (xOG > xRES) {
					errors.push_back((abs(xOG - xRES)) / (double)xOG);
				} else {
					errors.push_back((abs(xOG - xRES)) / (double)xRES);
				}
			}

			double max = 0;
			for (double error : errors) {
				if (error > max) {
					max = error;
				}
			}

			double avg = 1.0 * std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size();
			outputFileAvg << avg << std::endl;
			outputFileMax << max << std::endl;
			errors.clear();
		
		}

		outputFileMax.close();
		outputFileAvg.close();
	}
	return 0;
}