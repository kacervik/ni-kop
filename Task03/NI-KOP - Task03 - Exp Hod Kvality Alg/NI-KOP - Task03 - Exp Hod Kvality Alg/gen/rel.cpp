#include <iostream>
#include <fstream>
#include <numeric>
#include <vector>
#include <cmath>
#include <algorithm>
#include <bits/stdc++.h>
#include <cstdlib>
#include <string.h>

using namespace std;

int getNextInt(string* readLine) {//just reading file
    int position = readLine->find(" ");
    string token = readLine->substr(0, position);
    readLine = &(readLine->erase(0, position + 1));
    return stoi(token);
}

double avg1(vector<double> const &vector) {
    return accumulate(vector.begin(), vector.end(), 0.0) / vector.size();
}

double max(std::vector<double> &vector) {
    double max = 0.0;
    for (auto val : vector) {
        if (max < val) max = val;
    }
    return max;
}

int getMax(int x, int y) {
    if (x > y) {
        return x;
    } else {
        return y;
    }
}

double getRel(int result, int solution) {
    int max = getMax(result, solution);
    if (max == 0) {
        return 0.0;
    }
    int absVal = abs(result - solution);
    double returnVal = (double)absVal / max;
    return returnVal;
}

int main(int argc, char *argv[]) {
    ifstream resFile(argv[1]);
    ifstream solFile(argv[2]);
    ofstream writeFile(argv[3], std::ios_base::app);
    string tokenRes, tokenSol;
    vector<double> values;

    while(getline(resFile, tokenRes)) {
        getline(solFile, tokenSol);
        int id = getNextInt(&tokenRes);
        getNextInt(&tokenRes);
        while (true) {
            int solID = getNextInt(&tokenSol);
            if (solID == id) {
                getNextInt(&tokenSol);
                break;
            } else {
                getline(solFile, tokenSol);
            }
        }
        int result = getNextInt(&tokenRes);
        int solution = getNextInt(&tokenSol);

        double relative = getRel(result, solution);

        values.push_back(relative);
    }
    resFile.close();
    solFile.close();

    sort(values.begin(), values.end());
    auto const a = avg1(values);
    auto const maxVal = max(values);

    writeFile<<argv[4]<<" "<<a<<" "<<maxVal<<endl;

    writeFile.close();

    return 0;
}

