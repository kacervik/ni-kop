// NI-KOP - Task2 - Problem Batohu.cpp : Defines the entry point for the application.
//

// #include <crtdbg.h>
#include "Bag.h"
#include "Timer.h"

/*
	Read the file and process it in brute force calculations.
*/
bool processFile(std::ifstream& inputFile, const std::string& inputFileName) {
	inputFile.open(inputFileName, std::ios::in);
	if (!inputFile.is_open() || !inputFile) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	// TODO change method name
	double eps = 0.50;

	std::string outputFileNameResults = inputFileName.substr(0, inputFileName.length() - 4) + "_BnB_res.txt";
	// std::string outputFileNameResults= inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_res.txt";
	std::ofstream outputFileResults(outputFileNameResults);

	std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 4) + "_BnB_time.txt";
	// std::string outputFileNameTime = inputFileName.substr(0, inputFileName.length() - 8) + "FPTAS" + std::to_string(eps).substr(0, 4) + "_time.txt";
	std::ofstream outputFileTime(outputFileNameTime);

	while (inputFile.good()) {
		Bag bag;
		Item tmpItem;

		inputFile >> bag.ID >> bag.numberOfItems >> bag.bagCapacity;
		if (bag.ID < 0) {
			inputFile >> bag.minimalPriceOfItems;
		}

		for (int i = 0; i < bag.numberOfItems; ++i) {
			inputFile >> tmpItem.weight >> tmpItem.price;
			bag.items.push_back(tmpItem);
		}

		if (!inputFile.eof()) {
			Timer stopwatch;
			for (int i = 0; i < 3; ++i) {
				// bag.bruteForceMethod_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveBnBToFile(outputFileResults);
				// }
				// bag.printResult_ConstructiveAlgorithm();

				bag.branchAndBoundsMethod_KnapsackProblem();
				if (i == 0) {
					bag.saveBnBToFile(outputFileResults);
				}
				// bag.printResult_ConstructiveAlgorithm();

				// bag.greedyMethod_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveResult_GreedyAlgorithm(outputFileResults);
				// }
				// bag.printResult_GreedyAlgorithm();

				// int reduxResult = bag.greedyReduxMethod_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveResult_ReduxAlgorithm(reduxResult, outputFileResults);
				// }

				// bag.FPTAS_KnapsackProblem(eps);
				// bag.dynamicProgramming_KnapsackProblem();
				// if (i == 0) {
				// 	bag.saveResult_DynamicProgramming(outputFileResults);
				// }
				// bag.printResult_DynamicProgramming();
			}
			outputFileTime << (long long)(stopwatch.getElapsedTime()/3.0f) << std::endl;
		}
	}

	outputFileResults.close();
	return true;
}


int main(int argc, char** argv) {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// std::vector<std::string> instances = {"4", "8", "12", "16", "20", "24", "28"};
	// std::vector<std::string> instances = {"0.15", "0.30", "0.45", "0.60", "0.75", "0.90"};
	// std::vector<std::string> instances = {"100", "250", "400", "550", "700", "850", "1000"};
	// std::vector<std::string> instances = {"10", "25", "100", "150", "200", "350", "500", "750", "1000"};
	std::vector<std::string> instances = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
	// std::vector<std::string> instances = {"0.25", "0.5", "0.75", "1", "1.25", "1.5", "1.75", "2", "2.5", "3", "3.5", "4", "5", "6", "7"};
	// std::vector<std::string> types = {"bal", "light", "heavy"};
	// std::vector<std::string> types = {"corr", "strong", "uni"};
	std::vector<std::string> types = {""};
	std::string inputFileName;

	for (std::string number : instances) {
		for (std::string type : types) {
			inputFileName = "gen\\rob\\" + type + number + ".txt";

			std::ifstream inputFile;
			if (!processFile(inputFile, inputFileName)) {
				std::cout << "There was a problem processing your file." << std::endl;
				inputFile.close();
				return 1;
			}
			inputFile.close();
		}
	}

	return 0;
}
