#pragma once

#define _CRTDBG_MAP_ALLOC
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <climits>

struct Item {
	int weight = 0;
	int price = 0;
};

struct GreedyItem {
	int originalPosition;
	double ratio = 0;
	bool isAdded = false;
};

struct Bag {
	// General info
	int ID = 0;
	int numberOfItems = 0;
	int bagCapacity = 0;
	std::vector<Item> items;

	// Helping info
	int minimalPriceOfItems = 0;
	int maximalReachedPrice = -1;
	int currentMinWeightForMaxPrice = INT_MAX;

	// Stuff for Branch and Bounds
	std::vector<bool> configurationBnB;

	// Stuff for greey algorithm.
	std::vector<GreedyItem> greedyItems;
	int indexOfItemWithMaxPriceAndMinWeight = -1;

	// Stuff for dynamic programming.
	long int sumOfItems = 0;
	std::vector<bool> configurationDynamic;

	// Stuff for FPTAS
	std::vector<Item> itemsFTPAS;

	void printBagContents();

	// Brute Force
	void bruteForceMethod_KnapsackProblem();
	void buildTreeBruteForce(Item item, int depth, std::vector<bool> config);
	// void saveBFToFile(std::ofstream& outputFile);
	void printResult_ConstructiveAlgorithm();

	// Branch And Bounds
	void branchAndBoundsMethod_KnapsackProblem();
	void buildTreeBnB(Item item, int depth, std::vector<bool> config);
	void saveBnBToFile(std::ofstream& outputFile);

	// Greedy Method
	void greedyMethod_KnapsackProblem(bool fromRedux = false);
	void constructGreedyOrder(bool fromRedux = false);
	void determineSuitableItemsInGreedyOrder();
	void printResult_GreedyAlgorithm();
	void saveResult_GreedyAlgorithm(std::ofstream& outputFile);

	// Greedy Method - REDUX
	int greedyReduxMethod_KnapsackProblem();
	int compareGreedyAndReduxMethods();
	// void printResult_ReduxAlgorithm(int result);
	void saveResult_ReduxAlgorithm(int result, std::ofstream& outputFile);

	// Dynamic Programming
	long int pos(int a, int b);
	void dynamicProgramming_KnapsackProblem();
	void printResult_DynamicProgramming();
	void saveResult_DynamicProgramming(std::ofstream& outputFile);

	// Dynamic Programming FPTAS
	void FPTAS_KnapsackProblem(double eps = 0.05f);
};
