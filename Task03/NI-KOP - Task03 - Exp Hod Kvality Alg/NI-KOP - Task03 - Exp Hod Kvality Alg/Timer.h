#pragma once
#include <iostream>
#include <chrono>

/*
class Timer {
public:
	Timer() {
		start_m = std::chrono::high_resolution_clock::now();
	}

	double getElapsedTime() {
		end_m = std::chrono::high_resolution_clock::now();
		duration_m = end_m - start_m;
		// std::cout << duration_m.count() * 1000.0f << "ms" << std::endl;

		return (int)(duration_m.count() * 1000000.0f);
	}

private:
	std::chrono::time_point<std::chrono::steady_clock> start_m, end_m;
	std::chrono::duration<double> duration_m;
};
*/


struct Timer {
public:
	Timer() {
		startTimepoint_m = std::chrono::high_resolution_clock::now();
	}

	long long getElapsedTime() {
		endTimepoint_m = std::chrono::high_resolution_clock::now();

		start = std::chrono::time_point_cast<std::chrono::microseconds>(startTimepoint_m).time_since_epoch().count();
		stop = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint_m).time_since_epoch().count();

		duration_m = stop - start;

		// std::cout << duration_m << '\n';
		return duration_m;
	}

	std::chrono::time_point<std::chrono::high_resolution_clock> startTimepoint_m, endTimepoint_m;
	long long start, stop, duration_m;
private:
};

