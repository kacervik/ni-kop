#pragma once
#include <vector>

class Literal {
public:
	int index;
	bool value;
};

class Clause {
public:
	Literal literals[3];
	bool isTrue = false;
	// double weight = -1;
};

class SATProblem {
public:
	int nn = -1;
	int mm = -1;
	std::vector<double> weights;
	std::vector<Clause> clauses;
};
