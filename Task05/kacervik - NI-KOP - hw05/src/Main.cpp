#include <iostream>
#include <string>
#include <fstream>

#include "SAT.h"
#include "SATSolver.h"

constexpr auto normalizationRange = 1000.0f;

bool readInstanceFile(SATProblem& cnf, const std::string& instanceFileName) {
	std::ifstream inputInstanceFile;

	// Check if the file exists.
	inputInstanceFile.open(instanceFileName, std::ios::in);
	if (!inputInstanceFile.is_open() || !inputInstanceFile) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	// Read file and ignore everything till useful info.
	std::string tmp;
	inputInstanceFile >> tmp;
	while (tmp != "p") {
		std::getline(inputInstanceFile, tmp);
		inputInstanceFile >> tmp;
	}

	// skip instance type and read number of instances and number of clauses.
	inputInstanceFile >> tmp >> tmp;
	cnf.nn = std::stoi(tmp);
	inputInstanceFile >> tmp;
	cnf.mm = std::stoi(tmp);

	// Skip instance name for now.
	inputInstanceFile >> tmp;
	std::getline(inputInstanceFile, tmp);

	// Readn weights.
	inputInstanceFile >> tmp >> tmp;
	while (tmp != "0") {
		cnf.weights.push_back((double)std::stoi(tmp));
		inputInstanceFile >> tmp;
	}

	// Skip range line
	inputInstanceFile >> tmp;
	std::getline(inputInstanceFile, tmp);

	// Get clause values.
	inputInstanceFile >> tmp;
	while (tmp != "%") {
		Clause clause;
		for (int i = 0; i < 3; ++i) {
			if (std::stoi(tmp) >= 0) {
				clause.literals[i].value = true;
				clause.literals[i].index = std::stoi(tmp);
			} else {
				clause.literals[i].value = false;
				clause.literals[i].index = abs(std::stoi(tmp));
			}

			inputInstanceFile >> tmp; // Reads 0 on the last iteration.
		}
		inputInstanceFile >> tmp; // After the last iteration, reads first literal of next clause.

		cnf.clauses.push_back(clause);
	}

	inputInstanceFile.close();
	return true;
}

void normalizeWeights(SATProblem& instance) {
	double max = 0;
	for (int i = 0; i < instance.weights.size(); ++i) {
		if (instance.weights[i] > max) {
			max = instance.weights[i];
		}
	}

	for (int i = 0; i < instance.weights.size(); ++i) {
		instance.weights[i] = (instance.weights[i] / max) * normalizationRange;
	}
}

int main() {
	std::string number = "20-91";
	std::string fileName = "wuf" + number.substr(0, 2) + "-0114";
	std::string variation = "A";

	std::ofstream outputFile("Graphs/" + variation + " - " + fileName + "-weightsFinal.txt");
	for (int i = 0; i < 25; ++i) {
		SATProblem instance;
		std::string instanceFileName = "Instances/wuf-" + variation + "/wuf" + number + "-" + variation +"/" + fileName + "-A.mwcnf";
		readInstanceFile(instance, instanceFileName);

		// normalizeWeights(instance);
		SATSolver solver;

		SATState bestState = solver.simulatedAnnealingSolver(instance, i, variation + " - " + fileName);
		std::cout << i << " ";
		bestState.printState(fileName);
		outputFile << bestState.configWeight << std::endl;
	}
	outputFile.close();
 	return 0;
}
