#pragma once

#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <limits>

#include "SAT.h"
#include "SATState.h"

class SATSolver {
private:
	double temperature_m;
	const double temperatureMultiplier = 1.5f;
	SATState tryNeighbourState(SATProblem& instance, SATState& state);
	double initializeTemperature(SATProblem& instance, SATState& state);

public:
	SATState simulatedAnnealingSolver(SATProblem instance, const int& fileIndex, const std::string& variation);
};