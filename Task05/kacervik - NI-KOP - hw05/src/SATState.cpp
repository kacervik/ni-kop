#include "SATState.h"

double SATState::getCost(const double numberOfClauses) {
	if (numberOfClauses > satisfiableClauseNum) {
		return satisfiableClauseNum - numberOfClauses;
	} else {
		return configWeight;
	}
}

void SATState::generateRandomStartConfiguration(const int& nn) {
	std::random_device dev;
	std::mt19937 rng(dev());

	for (int i = 0; i < nn; ++i) {
		std::uniform_real_distribution<double> dist(0, 1);
		if (dist(rng) > 0.5f) {
			this->configuration[i] = true;
		}
	}
}

void SATState::printState(const std::string& instanceName) {
	std::cout << instanceName << " ";
	std::cout << this->configWeight << " ";
	for (int i = 0; i < this->configuration.size(); ++i) {
		if (this->configuration[i]) {
			std::cout << i + 1 << " ";
		} else {
			std::cout << "-" << i + 1 << " ";
		}
	}
	std::cout << "0\n";
}
