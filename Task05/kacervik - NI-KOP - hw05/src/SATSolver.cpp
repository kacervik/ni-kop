#include <random>

#include "SATSolver.h"

constexpr double frozenConstant = 0.1f;
constexpr double equilibriumConstant = 0.5;
constexpr double coolingRate = 0.99f;
constexpr int tailStop = 10;

bool frozen(const double& temperature) {
	return temperature > frozenConstant;
}

bool equilibrium(int& iterations, int size) {
	return iterations < (size * equilibriumConstant);
}

void cool(double& temp) {
	temp *= coolingRate;
}

void recalculateWeights(SATProblem& instance, SATState& state) {
	bool isTrue = false;
	for (int i = 0; i < instance.mm; ++i) { // For each clause
		// instance.clauses[i].weight = 0;
		for (int j = 0; j < 3; ++j) { // and each literal
			// Decreasing index by one, so that its aligned with arrays.
			// If configuration on literal index in clause is 1 and the value of literal is true, then its plausible literal
			if (state.configuration[instance.clauses[i].literals[j].index - 1] == 1 && instance.clauses[i].literals[j].value == true) {
				isTrue = true;
				// instance.clauses[i].weight += instance.weights[instance.clauses[i].literals[j].index - 1];
			}

			// If configuration on literal index in clause is 0 and the value of literal is false, then its plausible literal
			if (state.configuration[instance.clauses[i].literals[j].index - 1] == 0 && instance.clauses[i].literals[j].value == false) {
				isTrue = true;
				// instance.clauses[i].weight += instance.weights[instance.clauses[i].literals[j].index - 1];
			}
		}

		if (isTrue) {
			instance.clauses[i].isTrue = true;
		} else {
			instance.clauses[i].isTrue = false;
		}
		isTrue = false;
	}

	// Get clauses and config weights.
	// state.clausesWeight = 0;
	state.configWeight = 0;
	int satisfiableN = 0;
	for (int i = 0; i < instance.mm; ++i) {
		if (instance.clauses[i].isTrue) {
			// state.clausesWeight += instance.clauses[i].weight;
			++satisfiableN;
		}
	}
	for (int i = 0; i < instance.nn; ++i) {
		if (state.configuration[i]) {
			state.configWeight += instance.weights[i];
		}
	}

	// Get number of satisfiable clauses.
	state.satisfiableClauseNum = satisfiableN;
	// if (satisfiableN == instance.mm) {
	// 	state.isConfigViable = true;
	// }
}

void generateRandomNeighbour(SATProblem& instance, SATState& state) {
	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<int> dist(0, state.configuration.size() - 1);

	int position = dist(rng);
	if (state.configuration[position]) {
		state.configuration[position] = false;
		// state.clausesWeight -= instance.weights[position];
	} else {
		state.configuration[position] = true;
		// state.clausesWeight += instance.weights[position];
	}

	recalculateWeights(instance, state);
}

SATState SATSolver::tryNeighbourState(SATProblem& instance, SATState& state) {
	SATState newState = state;
	generateRandomNeighbour(instance, newState);

	if (newState.getCost(instance.mm) > state.getCost(instance.mm)) {
		return newState;
	}

	double delta = state.getCost(instance.mm) - newState.getCost(instance.mm);
	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_real_distribution<double> dist(0, 1);

	double tmp = -delta / (double)temperature_m;
	double x = dist(rng);
	double exp = std::exp(tmp);

	if (x < exp) {
		return newState;
	}

	// if (x < 0.005f) {
	// 	state.generateRandomStartConfiguration(instance.nn);
	// }
	
	return state;
}

double SATSolver::initializeTemperature(SATProblem& instance, SATState& state) {
	temperature_m = 1;
	double propability = 0.5;
	int numOfIterations = instance.nn;
	SATState newState(instance.nn);

	while (true) {
		while (true) {
			int nWorse = 0;
			for (int i = 0; i < numOfIterations; ++i) {
				newState = tryNeighbourState(instance, state);
		
				if (newState.getCost(instance.mm) < state.getCost(instance.mm)) {
					++nWorse;
				}
		
				state = newState;
			}
		
			double x = (double)nWorse / (double)numOfIterations;
			if (x >= propability) {
				break;
				// return temperature_m;
			}
			temperature_m *= temperatureMultiplier;
		}

		if (temperature_m > std::numeric_limits<double>::max()) {
			temperature_m = 1;
			propability *= 0.9;
			continue;
		} else {
			break;
		}
	}

	return temperature_m;
}

bool equals(SATState& lastState, SATState& currentState) {
	if (lastState.satisfiableClauseNum != currentState.satisfiableClauseNum) {
		return false;
	}

	if (lastState.configWeight != currentState.configWeight) {
		return false;
	}

	if (lastState.configuration != currentState.configuration) {
		return false;
	}

	return true;
}

SATState SATSolver::simulatedAnnealingSolver(SATProblem instance, const int& fileIndex, const std::string& variation) {
	int equilibriumIterations = 0, sameStateCounter = 0;
	SATState state(instance.nn), bestState(instance.nn), lastState(instance.nn);
	state.generateRandomStartConfiguration(instance.nn);
	temperature_m = initializeTemperature(instance, state);
	recalculateWeights(instance, state);

	// Graphs creation
	std::string outputFileNameGraphs = "Graphs/" + variation + "-clauses-" + std::to_string(fileIndex) + ".txt";
	std::string outputFileNameGraphs2 = "Graphs/" + variation + "-weights-" + std::to_string(fileIndex) + ".txt";
	std::ofstream outputFile;
	std::ofstream outputFile2;
	if (fileIndex == 0) {
		outputFile.open(outputFileNameGraphs);
		outputFile2.open(outputFileNameGraphs2);
	}

	while (frozen(temperature_m)) {
		while (equilibrium(equilibriumIterations, instance.mm)) {
			state = tryNeighbourState(instance, state);
			if (state.getCost(instance.mm) > bestState.getCost(instance.mm)) {
				bestState = state;
			}
			++equilibriumIterations;

			if (fileIndex == 0) {
				outputFile << state.satisfiableClauseNum << std::endl;
				outputFile2 << state.configWeight << std::endl;
			}
			// std::cout << state.satisfiableClauseNum << std::endl;
		}

		// If states are equal for too long, break the loop. (Helps with "tail" in the graph)
		if (equals(lastState, state)) {
			++sameStateCounter;
		} else {
			sameStateCounter = 0;
		}
		
		if (sameStateCounter > tailStop) {
			break;
		}

		cool(temperature_m);
		equilibriumIterations = 0;
		lastState = state;
	}

	outputFile.close();
	return bestState;
}
