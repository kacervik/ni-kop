#pragma once
#include <vector>
#include <random>
#include <iostream>

class SATState {
public:
	std::vector<bool> configuration;
	// bool isConfigViable = false;
	int satisfiableClauseNum = -1;
	// double clausesWeight = DBL_MIN;
	double configWeight = DBL_MIN;

	SATState(const int n) {
		for (int i = 0; i < n; ++i) {
			configuration.push_back(false);
		}
	}

	double getCost(const double numberOfClauses);
	void generateRandomStartConfiguration(const int& nn);
	void printState(const std::string& instanceName);
};