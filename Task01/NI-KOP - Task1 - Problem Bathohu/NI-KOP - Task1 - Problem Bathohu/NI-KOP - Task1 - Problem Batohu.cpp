﻿// NI-KOP - Task1 - Problem Batohu.cpp : Defines the entry point for the application.
//

#include "NI-KOP - Task1 - Problem Batohu.h"
#define filePath "Data\\ZR\\ZR"

bool cmp(Item& a, Item& b) {
	return (a.weight > b.weight);
}


/*
	Recursively build tree.
	Each left branch does not add new item.
	Each right branch does add new item dependant on the depth of the tree.
	Returns true if suitable configuration is found, else keep finding by returing false.
*/
bool buildTree(Bag& bag, Item item, unsigned int depth) {
	// If item is already over limit, dont continue.
	if (item.weight > bag.bagCapacity) {
		return false;
	}

	// If we are adding the last item.
	if (depth == bag.numberOfItems) {
		// And the configuration is suitable.
		if (item.price >= bag.minimalPriceOfItems && item.weight <= bag.bagCapacity) {
			return true;
		}
		return false;
	}

	// Check if adding aditional price will meet the minimum.

	unsigned int additionalPrice = 0;
	for (int i = depth; i < bag.numberOfItems; ++i) {
		additionalPrice += bag.items[i].price;
	}

	if ((item.price + additionalPrice) < bag.minimalPriceOfItems) {
		return false;
	}


	// Left branch
	if (buildTree(bag, item, depth + 1)) {
		return true;
	}

	// Right branch
	item.price += bag.items[depth].price;
	item.weight += bag.items[depth].weight;
	if (buildTree(bag, item, depth + 1)) {
		return true;
	}

	return false;
}


/*
	Create binary tree to form all combinations and return imidiately after finding suitable combination.
	Return true if suitable combination is found, false otherwise.
*/
bool bruteForceKnapsackProblem(Bag bag) {
		Item item;
		if (buildTree(bag, item, 0)) {
			return true;
		} else {
			return false;
		}
}


/*
	Read the file n times and process it in brute force calculations.
*/
bool processFile(std::ifstream& inputFile,const std::string& inputFileName) {
	std::string number = inputFileName.substr(10, 2);
	// TODO
	std::string resultOutputFileName = "Benchmark\\ZR" + number + "_result.txt";
	std::ofstream resultOutputFile(resultOutputFileName);

	std::chrono::time_point<std::chrono::high_resolution_clock> startTimepoint_m, endTimepoint_m;

	// number of iterations
	for (int iteration = 0; iteration < 3; ++iteration) {
		// TODO
		std::string benchmarkOutputFileName = "Benchmark\\ZR" + number + "." + std::to_string(iteration) + ".txt";
		std::ofstream benchmarkOutputFile(benchmarkOutputFileName);

		inputFile.open(inputFileName, std::ios::in);
		if (!inputFile.is_open() || !inputFile) {
			std::cout << "File does not exist or is corrupted." << std::endl;
			return false;
		}

		while (inputFile.good()) {
			Bag bag;
			Item tmpItem;
			inputFile >> bag.ID >> bag.numberOfItems >> bag.bagCapacity >> bag.minimalPriceOfItems;

			for (int i = 0; i < bag.numberOfItems; ++i) {
				inputFile >> tmpItem.weight >> tmpItem.price;
				bag.items.push_back(tmpItem);
			}

			// Sort the items.
			// std::sort(bag.items.begin(), bag.items.end(), cmp);

			if (!inputFile.eof()) {
				// Measure time.
				startTimepoint_m = std::chrono::high_resolution_clock::now();
				bool result = bruteForceKnapsackProblem(bag);
				endTimepoint_m = std::chrono::high_resolution_clock::now();

				if (result) {
					if (iteration == 0) {
						resultOutputFile << "1" << std::endl;
					}
				} else {
					if (iteration == 0) {
						resultOutputFile << "0" << std::endl;
					}
				}
				
				auto start = std::chrono::time_point_cast<std::chrono::microseconds>(startTimepoint_m).time_since_epoch().count();
				auto stop = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint_m).time_since_epoch().count();
				auto duration_m = stop - start;
				benchmarkOutputFile << duration_m << std::endl;
			}
		}
		benchmarkOutputFile.close();
		inputFile.close();
	}

	resultOutputFile.close();
	return true;
}


int main(int argc, char** argv) {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	std::string instances[] = {/*"04", "10", "15", "20",*/ "22", "25", "27"/*, "30", "32", "35","37", "40"*/};

	std::string inputFileName;
/*
	if (argv[1]) {
		inputFileName = argv[1];
		inputFileName = filePath + inputFileName + ".dat";
	} else {
		std::cout << "File name was not specified." << std::endl;
		return 1;
	}
*/
	for (auto n : instances) {
		inputFileName = filePath + n + "_inst.dat";

		std::ifstream inputFile;
		if (!processFile(inputFile, inputFileName)) {
			std::cout << "There was a problem processing your file." << std::endl;
			inputFile.close();
			return 1;
		}
	}

	return 0;
}
