﻿// NI-KOP - Task1 - Problem Batohu.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#define _CRTDBG_MAP_ALLOC
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <crtdbg.h>
#include <algorithm>
#include <chrono>

// TODO: Reference additional headers your program requires here.

struct Item {
	unsigned int weight = 0;
	unsigned int price = 0;
};

struct Bag {
	int ID = 0;
	unsigned short int numberOfItems = 0;
	unsigned int bagCapacity = 0;
	int minimalPriceOfItems = 0;
	std::vector<Item> items;

	void printBagContents() {
		std::cout << Bag::ID << " " << Bag::numberOfItems << " " << Bag::bagCapacity << " " << Bag::minimalPriceOfItems << " - ";
		for (Item item : Bag::items) {
			std::cout << item.weight << " " << item.price << " ";
		}
		std::cout << std::endl;
	}
};

/*
struct Timer {
public:
	Timer() {
		startTimepoint_m = std::chrono::high_resolution_clock::now();
	}

	long long Stop() {
		endTimepoint_m = std::chrono::high_resolution_clock::now();

		start = std::chrono::time_point_cast<std::chrono::microseconds>(startTimepoint_m).time_since_epoch().count();
		stop = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint_m).time_since_epoch().count();

		duration_m = stop - start;
	
		std::cout << duration_m << '\n';
		return duration_m;
	}

	std::chrono::time_point<std::chrono::high_resolution_clock> startTimepoint_m, endTimepoint_m;
	long long start, stop, duration_m;
private:
};
*/

struct Timer {
	Timer() {
		start_m = std::chrono::high_resolution_clock::now();
	}

	~Timer() {
		end_m = std::chrono::high_resolution_clock::now();
		duration_m = end_m - start_m;

		std::cout << duration_m.count() * 1000.0f << "ms" << std::endl;
	}

public:
	std::chrono::time_point<std::chrono::steady_clock> start_m, end_m;
	std::chrono::duration<double> duration_m;
};
